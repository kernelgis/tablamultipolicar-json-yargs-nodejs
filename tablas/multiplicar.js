const fs = require('fs');

function grabarBD(base, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(`Tabla-${base}.json`, data , (err) => {
      if (err) reject(err);
      else
        resolve(`Tabla-${base}.json`)
      // console.log('Archivo Creado');
    });
  })
}

function crearTabla(base) {
  return new Promise((resolve, reject) => {
    if(!Number(base)){
      reject(`la base ${base} no es un Número`);
      return;
    }
    const tabla = [];
    console.log(`Tabla de Multiplicar del ${base}`);
    for (let i = 0; i < 10; i++) {
      let resultado = {
        base: base,
        multiplicador: i,
        producto: base * i
      }
      tabla.push(resultado)
      console.log(`${base} * ${i} = ${base * i}`);
    }

    grabarBD(base, JSON.stringify(tabla))
      .then(r => {resolve(r)})
      .catch(e => {reject(e)});
    
  });
}

function listar(base) {
  const tabla = require(`../Tabla-${base}.json`)

  tabla.forEach(element => {
    console.log(`${element.base} * ${element.multiplicador} = ${element.producto}`);
  });

}


module.exports = {
  crearTabla,
  listar
}




