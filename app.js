// const multiplicar = require('./tablas/multiplicar');
const { crearTabla, listar } = require('./tablas/multiplicar');
const argv = require('yargs')
  .command('crear', 'Crea una tabla de Multiplicar segun Base indicada', {
    base: {
      demand: true,
      alias: 'b'
    }
  })
  .command('listar', 'lista una tabla de Multiplicar segun Base indicada', {
    base: {
      demand: true,
      alias: 'b'
    }
  })
  .help()
  .argv;

console.log(argv);
// console.log('comandos:', argv._); // comandos: [ 'crear' ]

let comando = argv._[0];

console.log(comando);
let base = argv.base;

switch (comando) {
  case 'crear':
    crearTabla(base)
      .then(r => { console.log(r); })
      .catch(e => { console.error(e); });
    break;

  case 'listar':
    listar(base);
    console.log('Comando para Listar un tabla');

    break;
  case 'modificar':
    console.log('Comando para Modificar una tabla');
    break;
  case 'borrar':
    console.log('Comando para borrar una tabla');
    break;

  default:
    console.log('Comando Desconocido');
    break;
}


// console.log('baseeee', argv.base);

// let base = process.argv[2].split('=')[1];






